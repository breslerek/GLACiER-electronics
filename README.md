# GLACiER-electronics

<img src="igluna.png"  width="200" height="200">
<img src="glacier.png"  width="200" height="200">

Electronic design for GLACiER project. The aim of the project was to desing and built a system that would allow for safe communication and real-time localisation of an astronaut during mission outside of habitat. IGLUNA 2019 habitat was designed and built at Glacier Paradise at Klien Matterhorn, to simulate living conditions in harsh environment, with sub or near zero temperatures. While on EVA, thera was nearly 15m of ice cover between habiatat and astronaut (missons were carried out on surface, while habitat was located inside the glacier). 

Prject work included:
- design of communication poles with independent power systems, which allowed for astronaut localisation. 
- astroanut "Pip-Boy" - user interface that allowed to send and receive commands. 
- main transmitter station with weather station. 
- through-ice transceiver (not available on this repo). 

Authors: Julia Wajoras, Michał Kazaniecki, Karol Bresler. 

Project software was written by Julia Wajoras and Michał Kazaniecki and is available [here](https://github.com/earter/GLACiER-soft).
